#!/usr/bin/env python
"""
# nmap_inventory.py - Generates an Ansible dynamic inventory using NMAP
# Author
Jose Vicente Nunez Zuleta (kodegeek.com@protonmail.com)
"""
import json
import argparse

hosts = {
    'myhost1': {
        'test': 'testvalue1'
    },
    'myhost2': {
        'test': 'testvalue2'
    },
    'myhost3': {
        'test': 'testvalue3'
    }

}

groups = {
    'all': {
        'children': [
            'ungrouped',
            'mygroup'
        ]
    },
    'ungrouped': {
        'hosts': ["myhost1"]
    },
    'mygroup': {
        'hosts': ["myhost2"]
    }
}

groups_with_meta = {
    '_meta': {
        'hostvars': hosts
    },
    'all': {
        'children': [
            'ungrouped',
            'mygroup'
        ]
    },
    'ungrouped': {
        'hosts': ["myhost1"],
        'hosts': ["myhost3"],
    },
    'mygroup': {
        'hosts': ["myhost2"]
    }
}

if __name__ == '__main__':

    arg_parser = argparse.ArgumentParser(
        description=__doc__,
        prog=__file__
    )
    arg_parser.add_argument(
        '--pretty',
        action='store_true',
        default=False,
        help="Pretty print JSON"
    )
    mandatory_options = arg_parser.add_mutually_exclusive_group()
    mandatory_options.add_argument(
        '--list',
        action='store',
        nargs="*",
        default="dummy",
        help="Show JSON of all managed hosts"
    )
    mandatory_options.add_argument(
        '--host',
        action='store',
        help="Display vars related to the host"
    )

    try:
        args = arg_parser.parse_args()
        if args.host:
            print(json.dumps(hosts[args.host]))
        elif len(args.list) >= 0:
            print(json.dumps(groups_with_meta, indent=args.pretty))
        else:
            raise ValueError("Expecting either --host $HOSTNAME or --list")

    except ValueError:
        raise
